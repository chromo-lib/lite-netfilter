import { readFileSync, writeFileSync } from "fs";
import { resolve } from "path";
import axios from 'axios';

const cleanDomain = (d) => d.replace(/^(\|\|)|(\^.*)|(\$.*)/g, '');

function cleanup(data: string, types = ['^$third-party']) {
  return data.split('\n')
    .filter(v => !v.endsWith('.') && (v.startsWith('||') && types.some(k => v.includes(k))) && (!v.includes('/') && !v.includes('*')) && !/\d\.\d/g.test(v))
    .map(k => cleanDomain(k));
}

function removeDuplicates(arr) {
  return arr.filter((item, index) => {
    item = item.replace(/(\?|\/)$/g, '');
    return arr.indexOf(item) === index
  });
}

async function thirdparties() {
  const easyprivacy = await axios.get('https://cdn.jsdelivr.net/gh/uBlockOrigin/uAssetsCDN@main/thirdparties/easyprivacy.txt');
  const ublock = await axios.get('https://filters.adtidy.org/extension/ublock/filters/3.txt');
  const Adguard = await axios.get('https://raw.githubusercontent.com/AdguardTeam/AdguardFilters/7b6ea94ddcfc0837e810ab7416f1e0452424aaea/SpywareFilter/sections/tracking_servers.txt');

  const oldRules = readFileSync(resolve('_filters', 'thirdParty', '_lite'), 'utf8').split('\n');
  const newRules = removeDuplicates([...cleanup(easyprivacy.data), ...cleanup(Adguard.data), ...cleanup(ublock.data), ...oldRules]).join('\n');
  writeFileSync(resolve('_filters', 'thirdParty', '1.txt'), newRules, 'utf8');
}

async function firstparties() {
  const badware = await axios.get('https://ublockorigin.github.io/uAssets/filters/badware.txt');
  const urlhaus = await axios.get('https://malware-filter.gitlab.io/urlhaus-filter/urlhaus-filter-ag-online.txt');

  const oldRules = readFileSync(resolve('_filters', 'firstParty', '_lite'), 'utf8').split('\n');
  const newRules = removeDuplicates([...cleanup(badware.data, ['^$doc', '^$all']), ...cleanup(urlhaus.data, ['^$doc', '^$all']), ...oldRules]).join('\n');
  writeFileSync(resolve('_filters', 'firstParty', '1.txt'), newRules, 'utf8');
}

async function firstpartiesTracking() {
  const resp = await axios.get('https://raw.githubusercontent.com/AdguardTeam/AdguardFilters/7b6ea94ddcfc0837e810ab7416f1e0452424aaea/SpywareFilter/sections/tracking_servers_firstparty.txt');
  const oldRules = readFileSync(resolve('_filters', 'firstParty_rd', '_lite'), 'utf8').split('\n');
  const newRules = removeDuplicates([...cleanup(resp.data, ['^']), ...oldRules]).join('\n');
  writeFileSync(resolve('_filters', 'firstParty_rd', '1.txt'), newRules, 'utf8');
}

export default async function updateRule() {
  thirdparties();
  firstparties();
  firstpartiesTracking();
}