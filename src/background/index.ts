let counter = 0;

const whiteList = [
  'openai.com',
  'www.google.com',
  'www.youtube.com',
  'fonts.gstatic.com',
  'mcs-va.tiktokv.com',
  'www.facebook.com'
];

function onErrorOccurred(details: chrome.webRequest.WebResponseErrorDetails) {
  if (!whiteList.some(v => details.url.includes(v))) {
    chrome.action.setBadgeText({ text: '' + counter++ });
    chrome.runtime.sendMessage({ details }).catch(e => { });
  }
}

function onStartup() {
  console.log('**--onStartup--**');
  chrome.webRequest.onErrorOccurred.addListener(onErrorOccurred, { urls: ["<all_urls>"] }, []);
}

function onAlarm(alarm: chrome.alarms.Alarm) {
  if (alarm.name === 'keepAlive') {
    console.log('Background task re-executed!');
  }
}

chrome.alarms.create('keepAlive', { periodInMinutes: 0.25 });
chrome.alarms.onAlarm.addListener(onAlarm);

function onSuspend() {
  console.log('**--onSuspend--**');
  chrome.webRequest.onErrorOccurred.removeListener(onErrorOccurred);
  chrome.alarms.onAlarm.removeListener(onAlarm);
  chrome.runtime.onStartup.removeListener(onStartup);
  chrome.runtime.onInstalled.removeListener(onStartup);
}

chrome.runtime.onSuspend.addListener(onSuspend);
chrome.runtime.onStartup.addListener(onStartup);
chrome.runtime.onInstalled.addListener(onStartup);
chrome.runtime.setUninstallURL('https://haikel-fazzani.deno.dev');