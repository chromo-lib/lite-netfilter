const errorListEL = document.getElementById('error-list')!;
const errorListFilterEL = document.getElementById('filter-error-list')!;

document.getElementById('form-search')?.addEventListener('submit', (e: any) => {
  e.preventDefault();
  const query = e.target.elements[0].value;
  const filter = e.target.elements[1].value;
  errorListFilterEL.innerHTML = '';

  [...errorListEL.children].forEach((el: any) => {
    if (filter === 'initiator' && el.dataset.initiator.includes(query)) errorListFilterEL.prepend(el)
    if (filter === 'url' && el.dataset.url.includes(query)) errorListFilterEL.prepend(el)
  })
});

document.getElementById('btn-clear')?.addEventListener('click', () => {
  errorListEL.innerHTML = '';
});

document.getElementById('btn-clear-filter')?.addEventListener('click', () => {
  errorListFilterEL.innerHTML = '';
});


function onMessage(msg: any, _, sendResponse) {
  sendResponse('');

  const details = msg.details as chrome.webRequest.WebResponseErrorDetails;

  if (details) {
    const li = document.createElement('li')
    li.innerHTML = `
      <h4 class="mt-0 red truncate" title="${details.url}">${details.url}</h4>
      <div class="d-flex gap-3 light small">
        <span title="initiator">${details.initiator}</span>
        <span title="method">${details.method}</span>
        <span title="type">${details.type}</span>
        <span title="error">${details.error}</span>
      </div>
    `;

    li.dataset.url = details.url;
    li.dataset.initiator = details.initiator;
    li.classList.add('border-bottom')
    errorListEL.prepend(li)
  }
}

chrome.runtime.onMessage.addListener(onMessage);